# openbis-training

Material for ETH openBIS training courses

Short URL: http://u.ethz.ch/YFHNg

Material for analysis training has been moved to a separate branch:
https://gitlab.ethz.ch/sis-rdm-training/openbis-training/-/tree/analysis_training
